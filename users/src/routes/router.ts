import { Router, Request, Response } from 'express';

import { UserModel } from '../models/models';
import { errorResponse } from '../global/utils';

const router = Router();

router.get('/', (req: Request, res: Response) => {
  res.json({
    ok: true,
    message: 'users microservices'
  });
});



router.get('/users/all', (req: Request, res: Response) => {
  UserModel
    .find({})
    .populate([
      { path: '_city', select: 'name' },
      { path: '_role', select: 'name' }
    ])
    .exec((err, users) => {
      const validator = (users.length > 0);

      if (err) {
        errorResponse(res, err.toString(), 500);
      } else if (!validator) {
        errorResponse(res, 'not found', 404);
      } else {
        res.status(200).json({ ok: true, users: users, length: users.length });
      }
    })
});


router.get('/user/:id', (req: Request, res: Response) => {
  const id = req.param('id');

  UserModel
    .findById(id)
    .populate([
      { path: '_city', select: 'name' },
      { path: '_role', select: 'name' }
    ])
    .exec((err, user) => {
      if (err) {
        errorResponse(res, err.toString(), 500);
      } else if (!user) {
        errorResponse(res, 'not found', 404);
      } else {
        res.status(200).json({ ok: true, users: user });
      }
    })
});


// Guardar un usaurio en la bd
router.post('/user', (req: Request, res: Response) => {
  const USER = req.body;

  new UserModel(USER).save((err) => {
    res.status(err ? 500 : 201)
      .json({
        ok: err ? false : true,
        [err ? 'error' : 'user']: err ? err.message : USER
      });
  });
});

export default router;