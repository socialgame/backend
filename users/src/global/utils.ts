import { Response } from 'express';

// return response error
export const errorResponse = (res: Response, message: string, status: number) => res.status(status).json({ ok: false, message: message });