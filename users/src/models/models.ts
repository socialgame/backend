import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

/**
 * @name RoleSchema
 * @table role
 */
const RoleSchema = new Schema(
    {
        name: {
            type: String,
            lowercase: true,
            required: true
        }
    },
    { timestamps: { createdAt: 'create_at', updatedAt: 'update_at' } }
);


/**
 * @name CountrySchema
 * @table country
 */
const CountrySchema = new Schema(
    {
        name: {
            type: String,
            lowercase: true,
            required: true
        }
    },
    { timestamps: { createdAt: 'create_at', updatedAt: 'update_at' } }
);



/**
 * @name CitySchema
 * @table city
 */
const CitySchema = new Schema(
    {
        name: {
            type: String,
            lowercase: true,
            required: true
        },
        _country: {
            type: ObjectId,
            ref: '',
            required: true
        }
    },
    { timestamps: { createdAt: 'create_at', updatedAt: 'update_at' } }
);



/**
 * @name UserSchema
 * @table user
 */
const UserSchema = new Schema(
    {
        username: {
            type: String,
            lowercase: true,
            required: true,
            unique: true
        },
        email: {
            type: String,
            lowercase: true,
            required: true,
            unique: true
        },
        password: {
            type: String,
            lowercase: true,
            required: true
        },
        active: {
            type: Boolean,
            default: true
        },
        _city: { type: ObjectId, ref: 'city' },
        _role: { type: ObjectId, ref: 'role' },
    },
    { timestamps: { createdAt: 'create_at', updatedAt: 'update_at' } }
);

// References
export const RoleModel = mongoose.model('role', RoleSchema, 'role');
export const CountryModel = mongoose.model('country', CountrySchema, 'country');
export const CityModel = mongoose.model('city', CitySchema, 'city');
export const UserModel = mongoose.model('user', UserSchema, 'user');