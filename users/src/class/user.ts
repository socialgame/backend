
export default class User {
    public username: string;
    public email: string;
    public password: string;
    public active: boolean;
    public _city: string;
    public _role: string;

    constructor(user: User) {
        this.username = user.username;
        this.email = user.email;
        this.password = user.password;
        this.active = user.active;
        this._city = user._city;
        this._role = user._role;
    }
}